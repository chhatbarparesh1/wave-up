# FAQ

1. [WaveUp can't wake up my device after a while (or at all)](#waveup-cant-wake-up-my-device-after-a-while-or-at-all)
2. [WaveUp can't lock my device since the latest Android upgrade (or at all)](#waveup-cant-lock-my-device-since-the-latest-android-upgrade-or-at-all)
3. [I can't uninstall WaveUp](#i-cant-uninstall-waveup)
4. [WaveUp doesn't work on my Samsung S10, OnePlus 7 Pro or some modern device (with the proximity sensor "hidden" behind the screen)](#waveup-doesnt-work-on-my-samsung-s10-oneplus-7-pro-or-some-modern-device-with-the-proximity-sensor-hidden-behind-the-screen)

### WaveUp can't wake up my device after a while (or at all)

Your device probably has some kind of battery manager or task killer that's messing up with the app.

Please check this website (it contains useful information on how to disable these in the most common devices):

https://dontkillmyapp.com/?2

### WaveUp can't lock my device since the latest Android upgrade (or at all)

You probably upgraded to Android 9 or newer. There was a big change in WaveUp for Android 9 and it now uses *Accessibility Services*

You'll need to disable and re-enable the lock option and allow WaveUp to register as an Accessibility Service.

Alternatively, you could go to Android's `Settings → Accessibility Services` and activate WaveUp from there.

### I can't uninstall WaveUp

On Android versions prior to 9, WaveUp could only lock the screen using *Device Admin* rights. This makes it a little more cumbersome to uninstall. The easiest way is to use the button at the bottom of WaveUp itself.

Note: the button isn't available in Android 9 and newer, as the app can be uninstalled like any other.

### WaveUp doesn't work on my Samsung S10, OnePlus 7 Pro or some modern device (with the proximity sensor "hidden" behind the screen)

I am afraid Samsung S10 is very particular in this case. Apparently, it doesn't allow apps to access the proximity sensor when the screen is turned on. Or it doesn't work correctly, I am not sure. Unfortunately, like this WaveUp can't lock the display.

According to this website, the vendors haven't been able to "hide" the proximity sensor under the display correctly: https://piunikaweb.com/2019/06/13/oneplus-7-pro-proximity-sensor-demystified-how-it-works-or-gets-buggy/
