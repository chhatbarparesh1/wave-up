New in 3.2.8
★ Update some translations.
★ Small bug fix (MainActivity leak).

New in 3.2.7
★ Fix compatibility with older Androids.
★ Update some translations.
★ Vibration time before locking is now configurable.

New in 3.2.6
★ Add Galician translation.
★ Fix 'Privacy Policy' title colour.

New in 3.2.5
★ Remove "send debug logs to dev" option.

New in 3.2.4
★ Update some translations.
